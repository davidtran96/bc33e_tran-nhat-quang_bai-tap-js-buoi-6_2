function kiemTraSoNguyenTo(x) {
  if (x < 2) return false;
  for (var i = 2; i <= Math.sqrt(x); i++) {
    if (x % i == 0) {
      return false;
    }
  }
  return true;
}

function tinhSoNguyenTo() {
  var so = document.getElementById("txt-number").value * 1,
    a = "";
  for (var i = 1; i <= so; i++) {
    if (kiemTraSoNguyenTo(i)) {
      a += i + " ";
    }
  }
  document.getElementById("result").innerHTML = a;
}
